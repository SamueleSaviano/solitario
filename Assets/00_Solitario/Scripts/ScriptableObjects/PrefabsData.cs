﻿using UnityEngine;

[CreateAssetMenu(fileName = "Prefabs Data", menuName = "Data/Prefabs Data")]
public class PrefabsData : ScriptableObject
{
    public GameObject card;
    public GameObject cardBase;
}
