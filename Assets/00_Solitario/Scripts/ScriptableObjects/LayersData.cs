﻿using UnityEngine;

[CreateAssetMenu(fileName = "Layers Data", menuName = "Data/Layers Data")]
public class LayersData : ScriptableObject
{
    public LayerMask cardLayer;
    public LayerMask deckLayer;

    public static int GetMask(LayerMask p_layer)
    {
        return LayerMask.GetMask(LayerMask.LayerToName(p_layer));
    }
}
