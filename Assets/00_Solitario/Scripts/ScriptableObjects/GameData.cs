﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Game Data", menuName = "Data/Game Data")]
public class GameData : ScriptableObject
{
    public List<ScriptableObject> data = new List<ScriptableObject>();

    [Space]

    public int cardColumns;
    public int maxDeckShowedCards;
    public float cardFlipTime;
    public float colimnCardsOffset;

    [Space]

    public int scorePerMove;
    public int scoreRemovedForDeckReset;

    [Space]

    public float hScreenOffset;

    [Space]

    public float holdInputTime;

    public static T GetData<T>() where T : ScriptableObject
    {
        return (T)GameSingleton.GameData.data.Find(_x => _x.GetType() == typeof(T));
    }
}
