﻿using System.Linq;
using UnityEngine;
using static GameSingleton;

[CreateAssetMenu(fileName = "Sprites Data", menuName = "Data/Sprites Data")]
public class SpritesData : ScriptableObject
{
    public Color red;
    public Color black;

    [Space]

    public SuitSprite[] cardSuits;
    public Sprite[] cardNumbers;

    public Sprite GetCardNumber(int p_number)
    {
        return this.cardNumbers[p_number - 1];
    }

    public Sprite GetCardSuit(CardSuit p_suit)
    {
        return this.cardSuits.ToList().Find(_x => _x.suit == p_suit).sprite;
    }

    public Color GetCardColor(CardSuit p_suit)
    {
        switch (p_suit)
        {
            case CardSuit.Hearts:
            case CardSuit.Tiles:
                return this.red;

            case CardSuit.Clovers:
            case CardSuit.Pikes:
                return this.black;
        }

        return Color.white;
    }

    public CardAspect GetCardSprites(int p_number, CardSuit p_suit)
    {
        return new CardAspect()
        {
            cardSuit = p_suit,
            color = this.GetCardColor(p_suit),
            number = this.GetCardNumber(p_number),
            suit = this.GetCardSuit(p_suit),
            value = p_number
        };
    }
}
