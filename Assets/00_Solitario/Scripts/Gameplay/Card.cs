﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameSingleton;

public class Card : MonoBehaviour
{
    #region Private

    private SpriteRenderer _renderer;
    private BoxCollider2D _collider;
    private TweenRotation _tweenRotation;
    private TweenPosition _tweenPosition;

    private SpriteRenderer[] _children;

    private Vector3 _startPosition;
    private Vector3 _size;

    private Card _collidingCard;
    private CardCollector _collidingCollector;
    private CardCollector _selectedCollector;

    private int _maxCardValue;
    private int _previousBaseDepth;
    private bool _callTwennCallback;

    #endregion

    #region Public

    [HideInInspector]
    public List<Card> cardChildren = new List<Card>();
    [HideInInspector]
    public List<CardCollector> previousCollectors = new List<CardCollector>();

    [HideInInspector]
    public CardCollector collector;

    [HideInInspector]
    public Vector3 currentPosition;    

    [HideInInspector]
    public CardParent parent;
    [HideInInspector]
    public CardSuit suit;
    [HideInInspector]
    public CardColor color;
    [HideInInspector]
    public CardOrientation orientation;
    [HideInInspector]
    public CardOrientation startOrientation;

    [Space]

    public SpriteRenderer number;
    public SpriteRenderer[] suitSprites;

    [HideInInspector]
    public int value;
    [HideInInspector]
    public bool canAttach;
    [HideInInspector]
    public bool isChild;

    #endregion

    #region Getters

    public Vector3 size
    {
        get
        {
            this._size = this._renderer.sprite.bounds.size;
            return this._size * this.transform.localScale.x;
        }
    }

    public Vector3 halfSize => this.size * 0.5f;
    public Vector3 colliderSize => this._collider.size;

    public CardCollector lastCollector => this.previousCollectors.Last();

    public float xPos => this.transform.position.x;
    public float yPos => this.transform.position.y;

    public float xLocPos => this.transform.localPosition.x;
    public float yLocPos => this.transform.localPosition.y;

    public float tweenDuration => this._tweenRotation.duration;

    public bool isFlipping => this._tweenRotation.enabled;

    public bool OverlapCard(Card p_other)
    {
        return this.xPos.IsInRange(p_other.xPos - this.halfSize.x, p_other.xPos + this.halfSize.x) &&
               this.yPos.IsInRange(p_other.yPos - this.size.y, p_other.yPos + this.size.y);
    }

    #endregion

    private void Awake()
    {
        this._renderer = this.GetComponent<SpriteRenderer>();
        this._collider = this.GetComponent<BoxCollider2D>();

        this._tweenRotation = this.GetComponent<TweenRotation>();
        this._tweenPosition = this.GetComponent<TweenPosition>();

        this._children = this.GetComponentsInChildren<SpriteRenderer>();

        this._maxCardValue = GameData.GetData<SpritesData>().cardNumbers.Length;

        this._tweenPosition.enabled = false;
        this._tweenRotation.enabled = false;
        this._tweenRotation.AddOnFinished(() => 
        {
            if (this._callTwennCallback)
            {
                this.SwitchOrientation();
                this.SetStartPos();

                this.ToggleCollider(this.orientation == CardOrientation.Front);
            }

            this.ResetTween();
        });
    }

    public void SetUp(CardOrientation p_orientation, CardAspect p_aspect)
    {
        this.orientation = p_orientation;
        this.startOrientation = this.orientation;

        this.suit = p_aspect.cardSuit;
        this.value = p_aspect.value;

        switch (this.suit)
        {
            case CardSuit.Hearts:
            case CardSuit.Tiles:
                this.color = CardColor.Red;
                break;

            case CardSuit.Clovers:
            case CardSuit.Pikes:
            default:
                this.color = CardColor.Black;
                break;
        }

        this.ToggleCollider(this.orientation == CardOrientation.Front);

        this.SetAspect(p_aspect);

        this.ResetTween();
        this.Flip(CardParent.Column, true);
    }

    private void SwitchOrientation()
    {
        this.orientation = this.orientation == CardOrientation.Front ? CardOrientation.Back : CardOrientation.Front;
    }

    public void SetStartPos()
    {
        this._startPosition = this.transform.localPosition;
    }

    public void BackToStartPos()
    {
        this.transform.localPosition = this._startPosition;
    }

    public void SetAspect(CardAspect p_aspect)
    {
        this.number.sprite = p_aspect.number;
        this.number.color = p_aspect.color;

        foreach (SpriteRenderer f_rend in this.suitSprites)
        {
            f_rend.sprite = p_aspect.suit;
        }
    }

    public void Flip(CardParent p_parent, bool p_immediate = false)
    {
        this._callTwennCallback = !p_immediate;

        this._tweenRotation.duration = p_immediate ? 0.001f : GameSingleton.GameData.cardFlipTime;
        this._tweenRotation.enabled = true;

        if(p_parent == CardParent.Deck)
        {
            this.Slide();
        }
    }

    public void Slide()
    {
        this._tweenPosition.enabled = true;
    }

    public void Move()
    {
        this.currentPosition.x = InputManager.InputPosition.x;
        this.currentPosition.y = InputManager.InputPosition.y;
        this.transform.position = this.currentPosition;

        this.CheckAttach();
    }

    private bool CanCheckCollector(CardCollector p_collector)
    {
        return this.collector != p_collector && p_collector.CanAttachCard(this);
    }

    private void CheckAttach()
    {
        for (int i = 0; i < CardsManager.Collectors.Count; i++)
        {
            if (this.CanCheckCollector(CardsManager.Collectors[i]))
            {
                this._collidingCollector = CardsManager.Collectors[i];

                switch (CardsManager.Collectors[i].type)
                {
                    case CardParent.Column:

                        if (this._collidingCollector.hasCards)
                        {
                            this._collidingCard = this._collidingCollector.lastCard;
                            this.canAttach = this._collidingCard.color != this.color &&
                                             this._collidingCard.value == this.value + 1;
                        }
                        else
                        {
                            this.canAttach = this.value == this._maxCardValue;
                        }

                        break;
                    case CardParent.Base:

                        this.canAttach = this.suit == CardsManager.Collectors[i].GetBase().suit &&
                                         this.value == CardsManager.Collectors[i].GetBase().maxValue + 1 &&
                                         this.cardChildren.Count == 0;

                        break;

                    case CardParent.Deck:

                        this.canAttach = false;
                        break;
                }

                if (GameSingleton.GameManager.canAlwaysAttach)
                {
                    this.canAttach = true;
                }

                break;
            }
            else
            {
                this._collidingCollector = null;
                this._collidingCard = null;

                this.canAttach = false;
            }
        }
    }

    public void GetCards()
    {
        if (this.collector.lastCard != this)
        {
            for (int i = this.collector.cards.IndexOf(this); i < this.collector.cards.Count; i++)
            {
                this.collector.cards[i].transform.SetParent(this.transform);
                this.collector.cards[i].ToggleCollider(false);

                if (this.collector.cards[i] != this)
                {
                    this.collector.cards[i].isChild = true;
                }

                this.cardChildren.Add(this.collector.cards[i]);
            }
        }

        this.SortCards();
    }

    public void ReleaseCards()
    {
        if (this.parent == CardParent.Column)
        {
            for (int i = 0; i < this.cardChildren.Count; i++)
            {
                this.collector.Attach(this.cardChildren[i]);
                this.cardChildren[i].ToggleCollider(true);
                this.cardChildren[i].isChild = false;
            }

            this.SortCards();
            this.cardChildren.Clear();

            this.collector.SortCards();
        }
    }

    private void SortCards()
    {
        int _index = this._renderer.sortingOrder;

        for (int i = 0; i < this.cardChildren.Count; i++)
        {
            this.cardChildren[i].Sort(_index, out _index);
        }
    }

    private void ResetTween()
    {
        switch (this.orientation)
        {
            case CardOrientation.Back:

                this._tweenRotation.from.y = 180f;
                this._tweenRotation.to.y = 0;

                break;
            case CardOrientation.Front:

                this._tweenRotation.from.y = 0;
                this._tweenRotation.to.y = 180f;
                break;
        }

        this._tweenRotation.ResetToBeginning();
    }

    public void SetTweenPosition(Vector3 p_from, Vector3 p_to)
    {
        this._tweenPosition.from = p_from;
        this._tweenPosition.to = p_to;

        this._tweenPosition.ResetToBeginning();
    }

    public void Sort(int p_startIndex, out int o_finalIndex)
    {
        this._previousBaseDepth = this._renderer.sortingOrder;

        int _index = p_startIndex;
        this._renderer.sortingOrder = --_index;

        for (int i = 0; i < this._children.Length; i++)
        {
            this._children[i].sortingOrder = ++_index;
        }

        o_finalIndex = _index;
    }

    public void RevertSort()
    {
        this.Sort(this._previousBaseDepth, out int o_out);
    }

    public void Attach(bool p_undo = false)
    {
        this._selectedCollector = p_undo ? this.lastCollector : this._collidingCollector;

        if (this._selectedCollector != null)
        {
            this._selectedCollector.Attach(this, p_undo);
            this.parent = this._selectedCollector.type;

            this.ReleaseCards();            

            this._selectedCollector = null;
            this._collidingCard = null;
            this.canAttach = false;

            if (!p_undo)
            {
                CardsManager.AddMovedCard(this);
                GameManager.UpdateStats(GameSingleton.GameData.scorePerMove, 1);
            }
            else
            {
                GameManager.UpdateStats(-GameSingleton.GameData.scorePerMove, 0);
            }
        }
    }

    public void Detach()
    {
        switch (this.parent)
        {
            case CardParent.Deck:

                GameSingleton.CardsManager.deck.RemoveCard(this);
                break;

            case CardParent.Column:

                this.collector.Remove(this);

                for (int i = 0; i < this.cardChildren.Count; i++)
                {
                    this.collector.Remove(this.cardChildren[i]);
                }

                break;

            case CardParent.Base:

                this.collector.Remove(this);

                break;
        }
    }

    public void Select()
    {
        this.Sort(100, out int o_out);

        if (this.parent == CardParent.Column)
        {
            this.GetCards();
        }
    }

    public void Deselect()
    {
        this.RevertSort();
        this.BackToStartPos();
        this.ReleaseCards();
    }

    public void ToggleCollider(bool p_enable)
    {
        this._collider.enabled = p_enable;
    }

    public void AddPrevious(CardCollector p_collector)
    {
        this.previousCollectors.Add(p_collector);
    }

    public void RemovePrevious(CardCollector p_collector)
    {
        this.previousCollectors.Remove(p_collector);
    }

    [ContextMenu("Undo")]
    public void Undo()
    {
        if (this.lastCollector != null)
        {
            switch (this.lastCollector.type)
            {
                case CardParent.Deck:

                    this.lastCollector.Undo(this);

                    break;
                case CardParent.Column:

                    this.GetCards();
                    this.lastCollector.Undo();

                    break;
                case CardParent.Base:

                    this.collector.Undo();

                    break;
            }

            StartCoroutine(this.UndoCO());
        }
        else
        {
            if(this.collector.type == CardParent.Deck)
            {
                GameSingleton.CardsManager.deck.UndoFlip(this);
            }
        }
    }

    private IEnumerator UndoCO()
    {        
        this.transform.SetParent(this.lastCollector.transform);
        this.Sort(100, out int o_int);

        yield return new WaitUntil(() => this.transform.parent == this.lastCollector.transform);

        this.currentPosition = this.transform.localPosition;

        this._tweenPosition.AddOnFinished(() =>
        {
            this.Attach(true);

            if (this.lastCollector != null)
            {
                this.lastCollector.OnUndo(this);
            }

            this.RemovePrevious(this.lastCollector);

            this._tweenPosition.onFinished.Clear();
        });

        this.SetTweenPosition(this.currentPosition, this.lastCollector.availablePosition);
        this.Slide();
    }

    public void ResetCard()
    {
        this.transform.SetParent(GameSingleton.CardsManager.cardsPool);
        this.transform.localScale = Vector3.one;
    }

    #region TEST

    [ContextMenu("Flip")]
    public void FlipTest()
    {
        this.Flip(CardParent.Column);
    } 

    #endregion
}
