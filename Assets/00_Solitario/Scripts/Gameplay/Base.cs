﻿using UnityEngine;

public class Base : CardCollector
{
    private Vector3 _position;

    public CardSuit suit;
    public SpriteRenderer suitImage;

    public int maxValue
    {
        get
        {
            if(this.cards.Count == 0)
            {
                return 0;
            }
            else
            {
                return this.cards[this.cards.Count - 1].value;
            }
        }
    }

    public bool completed => this.cards.Count == CardsManager.MaxCardValue;

    protected override void Awake()
    {
        this.type = CardParent.Base;
        base.Awake();
    }

    public void SetUp(CardSuit p_suit)
    {
        this.suit = p_suit;
        this.suitImage.sprite = GameData.GetData<SpritesData>().GetCardSuit(p_suit);
    }

    public void SetMeasures(int p_index)
    {
        this._position.x = CardsManager.Columns[p_index].xPos;
        this._position.y = GameSingleton.CardsManager.deck.position.y;

        this.transform.position = this._position;
        this.transform.localScale = GameSingleton.CardsManager.deck.cards[0].transform.localScale;
    }

    public override void Attach(Card p_card, bool p_undo = false)
    {
        base.Attach(p_card, p_undo);
        this.SortCards();

        GameManager.CheckCompletion();
    }
}
