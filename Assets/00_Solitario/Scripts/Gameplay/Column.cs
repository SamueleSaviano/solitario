﻿using UnityEngine;

public class Column : CardCollector
{
    private Vector3 _position;

    protected override void Awake()
    {
        this.type = CardParent.Column;
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    [ContextMenu("Sort")]
    public override void SortCards()
    {
        base.SortCards();
    }

    public override void Attach(Card p_card, bool p_undo = false)
    {
        base.Attach(p_card, p_undo);
        this.SortCards();
    }

    public override void Remove(Card p_card)
    {
        base.Remove(p_card);

        if(this.hasCards && this.lastCard.orientation == CardOrientation.Back)
        {
            this.lastCard.Flip(CardParent.Column);
        }
    }
    
    public override void Undo(Card p_card = null)
    {
        base.Undo(p_card);

        if (this.hasCards && (this.lastCard.orientation == CardOrientation.Front && this.lastCard.startOrientation == CardOrientation.Back))
        {
            this.lastCard.Flip(CardParent.Column);
        }
    }

    public void ResetPosition()
    {
        this._position = this.transform.localPosition;
        this._position.y = 0;

        this.transform.localPosition = this._position;
    }
}
