﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameSingleton;

public class Deck : CardCollector
{
    #region Private

    private BoxCollider2D _collider;

    private Card _flippingCard;
    private Card _cardToMove;

    private List<Card> _coveredCards = new List<Card>();
    private List<Card> _uncoveredCards = new List<Card>();
    private List<int> _movesAtReset = new List<int>();

    private Vector2 _tweenStartPos;
    private Vector2 _tweenTargetPos;

    #endregion

    #region Public

    public GameObject back;

    [Space]

    [HideInInspector]
    public Vector3 position;

    public static int CardsToFlip = 1;

    #endregion

    #region Getters

    public override Vector3 availablePosition
    {
        get
        {
            this._lastPosition = Vector3.zero;
            this._lastPosition.x = -this.cards[0].size.x - this.Offset(this._uncoveredCards.Count);

            return this._lastPosition;
        }
    }

    private int _maxShowedCards => GameSingleton.GameData.maxDeckShowedCards;

    private float Offset(int p_index)
    {
        return this.cards[0].size.x.Percentage(GameSingleton.GameData.colimnCardsOffset) * Mathf.Clamp(this._maxShowedCards - p_index, 1f, (float)this._maxShowedCards);
    }

    #endregion

    protected override void Awake()
    {
        this.type = CardParent.Deck;
        base.Awake();
    }

    public void SetUp(bool p_newGame)
    {
        if (p_newGame)
        {
            do
            {
                GameSingleton.CardsManager.SetCard(GameSingleton.CardsManager.GetPooledCard(GameSingleton.CardsManager.pooledCards - 1), CardParent.Deck);
            }
            while (GameSingleton.CardsManager.pooledCards > 0);
        }
        else
        {
            for (int i = 0; i < CardsManager.LastDeckCards.Count; i++)
            {
                GameSingleton.CardsManager.SetCard(GameSingleton.CardsManager.GetPooledCard(GameSingleton.CardsManager.pooledCards - 1), CardParent.Deck, null, 0, 0, i);
            }
        }

        CardsManager.SortCards(this.cards);

        foreach (Card f_card in this.cards)
        {
            this.CoverCard(f_card);
        }

        this.back.transform.localScale = this.cards[0].transform.localScale;

        this.SetPosition();
        this.SetCollider();
    }

    public void ResetDeck()
    {
        this.cards.Clear();
        this._coveredCards.Clear();
        this._uncoveredCards.Clear();
    }

    private void SetPosition()
    {
        this.position.x = CameraManager.RightLimit - this.cards[0].halfSize.x;
        this.position.y = CameraManager.TopLimit - CameraManager.Height.Percentage(15f) - this.cards[0].halfSize.y;

        this.transform.position = this.position;
    }

    private void SetCollider()
    {
        if (this._collider == null)
        {
            this._collider = this.GetComponent<BoxCollider2D>();
            this._collider.isTrigger = true;
            this._collider.size = new Vector2(1f / this.cards[0].colliderSize.y * this.cards[0].colliderSize.x, 1f);
        }
    }

    public void OnInput()
    {
        if (!CardsManager.IsCardFlipping)
        {
            if (this._coveredCards.Count > 0)
            {
                this.FlipCards();
            }
            else
            {
                this.ResetCards();
            }
        }
    }

    public void FlipCards(bool p_resetUndo = false)
    {
        if (!p_resetUndo)
        {
            for (int i = 0; i < Deck.CardsToFlip; i++)
            {
                this.Flip(this._coveredCards[i], this._uncoveredCards.Count);
                this.UncoverCard(this._coveredCards[i]);

                GameManager.UpdateStats(0, 1);
                CardsManager.AddMovedCard(this._flippingCard);
            }
        }
        else
        {
            bool _canMove;

            for (int i = 0; i < this._coveredCards.Count; i++)
            {
                _canMove = i >= this._coveredCards.Count - (this._maxShowedCards - 1);

                this.Flip(this._coveredCards[i], _canMove ? this._maxShowedCards - (this._coveredCards.Count - i) : 0, _canMove);
                this._uncoveredCards.Add(this._coveredCards[i]);
            }

            this._coveredCards.Clear();
            StartCoroutine(ResetColliders(this._uncoveredCards));
        }
    }

    private IEnumerator ResetColliders(List<Card> p_list)
    {
        yield return new WaitForSeconds(p_list.Last().tweenDuration);

        foreach (Card f_card in p_list)
        {
            f_card.ToggleCollider(false);
        }

        this.ToggleLastCollider(true);
    }

    private void Flip(Card p_card, int p_index, bool p_move = true)
    {
        this._flippingCard = p_card;

        this._tweenTargetPos = Vector2.zero;
        this._tweenTargetPos.x = -this._flippingCard.size.x - this.Offset(p_move ? p_index : 0);

        this._flippingCard.SetTweenPosition(Vector3.zero, this._tweenTargetPos);
        this._flippingCard.Flip(CardParent.Deck);
    }

    public void ResetCards()
    {
        CardsManager.AddMovedCard(this._uncoveredCards.Last());

        foreach (Card f_card in this.cards)
        {
            this.CoverCard(f_card, true);
        }

        this._movesAtReset.Add(GameManager.Moves);
        GameManager.UpdateStats(-GameSingleton.GameData.scoreRemovedForDeckReset, 1);
    }

    private void CoverCard(Card p_card, bool p_useTween = false)
    {
        if (p_useTween)
        {
            this._tweenStartPos = Vector2.zero;
            this._tweenStartPos.x = p_card.xLocPos;

            this._flippingCard = p_card;
            p_card.SetTweenPosition(this._tweenStartPos, Vector3.zero);
            p_card.Flip(CardParent.Deck);
        }

        this._coveredCards.Insert(0, p_card);

        if (this._uncoveredCards.Contains(p_card))
        {
            this._uncoveredCards.Remove(p_card);
        }
    }

    private void UncoverCard(Card p_card)
    {
        this.ToggleLastCollider(false);
        this.MoveUncoveredCards();

        this._uncoveredCards.Add(p_card);

        if (this._coveredCards.Contains(p_card))
        {
            this._coveredCards.Remove(p_card);
        }

        this.SortUncovered();
    }

    private void ToggleLastCollider(bool p_enable)
    {
        if (this._uncoveredCards.Count > 0)
        {
            this._uncoveredCards.Last().ToggleCollider(p_enable);
        }
    }

    private void SortUncovered()
    {
        int _index = 0;

        for (int i = 0; i < this._uncoveredCards.Count; i++)
        {
            this._uncoveredCards[i].Sort(++_index, out _index);
        }
    }

    private void MoveUncoveredCards(bool p_removed = false)
    {
        if (this._uncoveredCards.Count >= this._maxShowedCards)
        {
            for (int i = 0; i < this._maxShowedCards - 1; i++)
            {
                this._cardToMove = this._uncoveredCards[this._uncoveredCards.Count - (i + 1)];

                this._tweenTargetPos = Vector2.zero;
                this._tweenStartPos = Vector2.zero;
                this._tweenStartPos.x = this._cardToMove.xLocPos;

                if (!p_removed)
                {
                    this._tweenTargetPos.x = this._cardToMove.xLocPos - this.Offset(this._maxShowedCards - i);
                    this._cardToMove.SetTweenPosition(this._tweenStartPos, this._tweenTargetPos);
                }
                else
                {
                    this._tweenTargetPos.x = this._cardToMove.xLocPos + this.Offset(i + 2);
                    this._cardToMove.SetTweenPosition(this._tweenStartPos, this._tweenTargetPos);
                }

                this._cardToMove.Slide();
            }
        }
    }

    public void RemoveCard(Card p_card)
    {
        this._uncoveredCards.Remove(p_card);
        this.cards.Remove(p_card);

        this.ToggleLastCollider(true);
        this.MoveUncoveredCards(true);
    }

    public void UndoFlip(Card p_card)
    {
        if (this._movesAtReset.Count == 0 || (GameManager.Moves - 1) != this._movesAtReset.Last())
        {
            this.CoverCard(p_card, true);
            this.MoveUncoveredCards(true);
            this.ToggleLastCollider(true);
        }
        else
        {
            this.FlipCards(true);
            this._movesAtReset.Remove(this._movesAtReset.Last());
        }
    }

    public override void Undo(Card p_card = null)
    {
        base.Undo(p_card);
        this.MoveUncoveredCards();
    }

    public override void OnUndo(Card p_card)
    {
        base.OnUndo(p_card);

        this._uncoveredCards.Add(p_card);
        this.SortUncovered();
    }
}
