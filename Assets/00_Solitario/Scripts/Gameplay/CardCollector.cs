﻿using System.Collections.Generic;
using UnityEngine;

public class CardCollector : MonoBehaviour
{
    protected Base _baseComponent;
    protected Column _columnComponent;
    protected Deck _deckComponent;

    protected Vector3 _lastPosition;

    public CardParent type;

    public List<Card> cards = new List<Card>();   

    #region Getters

    public Column GetColumn() { return this._columnComponent; }
    public Base GetBase() { return this._baseComponent; }
    public Deck GetDeck() { return this._deckComponent; }

    public virtual Card lastCard { get { return this.cards.Last(); } }

    public virtual Vector3 availablePosition
    {
        get
        {
            this._lastPosition = Vector3.zero;

            if (this.cards.Count > 0)
            {
                this._lastPosition.y = this.lastCard.transform.localPosition.y - this.lastCard.size.y.Percentage(this._cardsOffset);
            }

            return this._lastPosition;
        }
    }

    protected float _cardsOffset
    {
        get
        {
            if (this.type == CardParent.Column)
            {
                return GameSingleton.GameData.colimnCardsOffset;
            }

            return 0;
        }
    }

    public float xPos => this.transform.position.x;
    public float yPos => this.transform.position.y;
    public bool hasCards => this.cards.Count > 0;

    public bool CanAttachCard(Card p_card)
    {
        if (this.hasCards)
        {
            return this.lastCard.OverlapCard(p_card);
        }

        return p_card.xPos.IsInRange(this.xPos - p_card.halfSize.x, this.xPos + p_card.halfSize.x) &&
               p_card.yPos.IsInRange(this.yPos - p_card.size.y, this.yPos + p_card.size.y);
    }

    #endregion
    
    protected virtual void Awake()
    {
        switch (this.type)
        {
            case CardParent.Column:

                this._columnComponent = this.gameObject.GetComponent<Column>();
                break;

            case CardParent.Base:

                this._baseComponent = this.gameObject.GetComponent<Base>();
                break;

            case CardParent.Deck:
                this._deckComponent = this.gameObject.GetComponent<Deck>();
                break;
        }
    }

    protected virtual void Start()
    {
        
    }

    [ContextMenu("Sort")]
    public virtual void SortCards()
    {
        CardsManager.SortCards(this.cards, this._cardsOffset);
    }

    public virtual void Attach(Card p_card, bool p_undo = false)
    {
        if (p_card.collector != this)
        {
            p_card.Detach();

            if (!p_undo && !p_card.isChild)
            {
                p_card.AddPrevious(p_card.collector);
            }

            p_card.collector = this;
        }

        p_card.transform.SetParent(this.transform);

        if (!this.cards.Contains(p_card))
        {
            this.cards.Add(p_card);
        }
    }

    public virtual void Remove(Card p_card)
    {
        this.cards.Remove(p_card);        
    }

    public virtual void Undo(Card p_card = null)
    {
        
    }

    public virtual void OnUndo(Card p_card)
    {

    }
}
