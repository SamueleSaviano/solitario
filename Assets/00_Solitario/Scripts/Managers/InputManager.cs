﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
#if UNITY_ANDROID && !UNITY_EDITOR

    public static Touch Touch => Input.GetTouch(0);

#endif

    public static Vector3 InputPosition
    {
        get
        {
#if UNITY_ANDROID && !UNITY_EDITOR

            return Camera.main.ScreenToWorldPoint(InputManager.Touch.position);

#else

            return Camera.main.ScreenToWorldPoint(Input.mousePosition);

#endif
        }
    }

#if UNITY_ANDROID && !UNITY_EDITOR

    public static bool IsTouching => Input.touchCount > 0;

#endif

    public static bool SingleInput
    {
        get
        {
#if UNITY_ANDROID && !UNITY_EDITOR

            return InputManager.IsTouching && InputManager.Touch.phase == TouchPhase.Began;

#else

            return Input.GetMouseButtonDown(0);

#endif
        }
    }

    public static bool InputDown
    {
        get
        {
#if UNITY_ANDROID && !UNITY_EDITOR

            return InputManager.IsTouching && InputManager.Touch.phase == TouchPhase.Stationary;

#else

            return Input.GetMouseButton(0);

#endif
        }
    }

    public static bool InputUp
    {
        get
        {
#if UNITY_ANDROID && !UNITY_EDITOR

            return InputManager.IsTouching && InputManager.Touch.phase == TouchPhase.Ended;

#else

            return Input.GetMouseButtonUp(0);

#endif
        }
    }
}
