﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static GameSingleton;

public class CardsManager : MonoBehaviour
{
    public static List<CardAspect> LastColumnsCards = new List<CardAspect>();
    public static List<CardAspect> LastDeckCards = new List<CardAspect>();

    public static List<CardAspect> AvailableCards = new List<CardAspect>();
    public static List<Card> AllCards = new List<Card>();

    public static List<CardCollector> Collectors = new List<CardCollector>();
    public static List<Column> Columns = new List<Column>();
    public static List<Base> Bases = new List<Base>();

    public static List<Move> MovedCards = new List<Move>();

    private SpritesData _spritesData;    

    public Transform cardsPool;
    public Deck deck;
    public Transform bases;
    public Transform columns;

    private Vector3 _columnsPosition;

    private bool _replay;

    #region Instancing

    private CardOrientation _cardOrientation;

    private GameObject _baseClone;
    private Base _base;

    private CardAspect _cardAspect;
    private GameObject _cardClone;
    private Card _card = null;

    private GameObject _columnClone;
    private Column _column;

    private Transform _cardParent;

    private Vector3 _cardScale = Vector3.one;
    private Vector3 _cardPos = Vector3.zero;
    private Vector3 _columnPos = Vector3.zero; 

    #endregion

    #region Getters

    public static Array Suits => Enum.GetValues(typeof(CardSuit));

    public static Card GetCard(CardAspect p_aspect)
    {
        return CardsManager.AllCards.Find(_x => _x.value == p_aspect.value && _x.suit == p_aspect.cardSuit);
    }

    public Card GetPooledCard(int p_index)
    {
        return this.cardsPool.GetChild(p_index).GetComponent<Card>();
    }

    public int pooledCards => this.cardsPool.childCount;

    private int _columns => GameSingleton.GameData.cardColumns;
    public float cardsOffset => CameraManager.HOffset * (this._columns - 1);
    private float _neededWidth => (CameraManager.Width - cardsOffset) / this._columns;

    public static int MaxCardValue => GameData.GetData<SpritesData>().cardNumbers.Length;

    public static bool IsCardFlipping
    {
        get
        {
            foreach (Card f_card in CardsManager.AllCards)
            {
                if (f_card.isFlipping)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public static bool Completed
    {
        get
        {
            foreach (Base f_base in CardsManager.Bases)
            {
                if (!f_base.completed)
                {
                    return false;
                }
            }

            return true;
        }
    }

    #endregion

    private void Awake()
    {
        GameSingleton.CardsManager = this;
    }

    private void Start()
    {
        this._spritesData = GameData.GetData<SpritesData>();

        this.SetAvailableCards();

        this.CreateCards();
        this.CreateColumns();
        this.CreateBases();

        this.SetTable();
    }

    private void SetTable()
    {
        this.SetColumns();
        this.deck.SetUp(!this._replay);
        this.SetBases();

        this.MoveColumns();
    }

    public void ResetCards(bool p_newGame)
    {
        this._replay = !p_newGame;

        if (p_newGame)
        {
            CardsManager.LastColumnsCards.Clear();
            CardsManager.LastDeckCards.Clear();
        }

        foreach (Card f_card in CardsManager.AllCards)
        {
            f_card.ResetCard();
        }

        foreach (CardCollector f_collector in CardsManager.Collectors)
        {
            f_collector.cards.Clear();
        }

        this.deck.ResetDeck();

        this.SetAvailableCards();
        this.SetTable();

        CardsManager.MovedCards.Clear();
    }

    public static void AddMovedCard(Card p_card)
    {
        if (CardsManager.MovedCards.Count == 0 ||
            CardsManager.MovedCards.Last().card != p_card)
        {
            CardsManager.MovedCards.Add(new Move()
            {
                card = p_card,
                moves = 1
            });
        }
        else
        {
            CardsManager.MovedCards.Last().moves++;
        }
    }

    private void SetAvailableCards()
    {
        CardsManager.AvailableCards.Clear();

        for (int i = 0; i < Suits.Length; i++)
        {
            for (int j = 0; j < this._spritesData.cardNumbers.Length; j++)
            {
                CardsManager.AvailableCards.Add(this._spritesData.GetCardSprites(j + 1, (CardSuit)Suits.GetValue(i)));
            }
        }
    }

    private void CreateCards()
    {
        for (int i = 0; i < CardsManager.AvailableCards.Count; i++)
        {
            this._cardClone = Instantiate(GameData.GetData<PrefabsData>().card, Vector3.zero, Quaternion.identity, this.cardsPool);
            this._card = this._cardClone.GetComponent<Card>();

            CardsManager.AllCards.Add(this._card);
        }
    }

    private void CreateColumns()
    {
        for (int i = 0; i < this._columns; i++)
        {
            this._columnClone = new GameObject($"Columns ({i})");
            this._columnClone.transform.SetParent(this.columns);
            this._column = this._columnClone.AddComponent<Column>();

            CardsManager.Columns.Add(this._column);
            CardsManager.Collectors.Add(this._column);
        }
    }

    private void CreateBases()
    {
        for (int i = 0; i < CardsManager.Suits.Length; i++)
        {
            this._baseClone = Instantiate(GameData.GetData<PrefabsData>().cardBase, Vector3.zero, Quaternion.identity, this.bases);
            this._base = this._baseClone.GetComponent<Base>();

            CardsManager.Bases.Add(this._base);
            CardsManager.Collectors.Add(this._base);
        }
    }

    private void SetColumns()
    {
        for (int i = 0; i < this.pooledCards; i++)
        {
            if (i < this._columns)
            {                
                this._column = CardsManager.Columns[i];

                for (int j = 0; j < i + 1; j++)
                {
                    this._card = this.GetPooledCard(i);
                    this.SetCard(this._card, CardParent.Column, this._column, j, i);
                }

                this.SetColumn(this._column, i);
            }
            else
            {
                break;
            }
        }
    }

    private void SetColumn(Column p_column, int p_index)
    {
        this._columnPos.y = 0;
        this._columnPos.x = Mathf.Lerp(CameraManager.LeftLimit + (this._card.size.x * 0.5f), CameraManager.RightLimit - (this._card.size.x * 0.5f),
                              Mathf.InverseLerp(0, this._columns - 1, p_index));

        p_column.gameObject.transform.position = this._columnPos;
        p_column.SortCards();
    }

    private void SetBases()
    {
        this.bases.position = new Vector3(0, this.deck.position.y, 0);

        for (int i = 0; i < CardsManager.Bases.Count; i++)
        {
            this._base = CardsManager.Bases[i];
            this._base.SetUp((CardSuit)CardsManager.Suits.GetValue(i));
            this._base.SetMeasures(i);
        }
    }

    public static void AddCollector(CardCollector p_collector)
    {
        if (!CardsManager.Collectors.Contains(p_collector))
        {
            CardsManager.AddCollector(p_collector);
        }
    }

    private void MoveColumns()
    {
        this._columnsPosition.y = this.deck.position.y - this.deck.cards[0].halfSize.y - (this.cardsOffset * 2f);
        this.columns.position = this._columnsPosition;

        foreach (Column f_col in CardsManager.Columns)
        {
            f_col.ResetPosition();
        }
    }

    public void SetCard(Card p_card, CardParent p_parent, Column p_column = null, int p_indexInColumn = 0, int p_columnLength = 0, int p_deckIndex = 0)
    {
        this._cardClone = p_card.gameObject;
        this._card = p_card;

        switch (p_parent)
        {
            case CardParent.Deck:

                this._cardParent = this.deck.transform;
                this._cardOrientation = CardOrientation.Back;

                break;

            case CardParent.Column:

                this._cardParent = p_column.transform;
                this._cardOrientation = (p_indexInColumn == p_columnLength) ? CardOrientation.Front : CardOrientation.Back;

                break;
        }

        this._cardClone.transform.SetParent(this._cardParent);
        this._card = this._cardClone.GetComponent<Card>();

        this._card.parent = p_parent;

        if (!this._replay)
        {
            this._cardAspect = CardsManager.AvailableCards.Random();
            CardsManager.AvailableCards.Remove(this._cardAspect);

            switch (p_parent)
            {
                case CardParent.Deck:

                    CardsManager.LastDeckCards.Add(this._cardAspect);
                    break;

                case CardParent.Column:

                    CardsManager.LastColumnsCards.Add(this._cardAspect);
                    break;
            }
        }
        else
        {
            switch (p_parent)
            {
                case CardParent.Deck:

                    this._cardAspect = CardsManager.LastDeckCards[p_deckIndex];
                    break;

                case CardParent.Column:

                    this._cardAspect = CardsManager.LastColumnsCards[p_indexInColumn + p_columnLength];
                    break;
            }            
        }

        this._card.SetUp(this._cardOrientation, this._cardAspect);

        this._cardScale = Vector3.one * this._neededWidth / this._card.size.x;
        this._cardClone.transform.localScale = this._cardScale;

        if(p_parent == CardParent.Column)
        {
            this._card.collector = this._column;
            p_column.cards.Add(this._card);
        }
        else
        {
            this._card.collector = this.deck;
            this.deck.cards.Add(this._card);
        }

        this._cardClone.name = $"Card_{this._card.value}_{this._card.suit}";
    }

    public static void SortCards(List<Card> p_cardsList, float p_offset = 0)
    {
        Vector3 _cardPos = Vector3.zero;
        Vector3 _cardScale = Vector3.one;

        int _index = 0;

        for (int i = 0; i < p_cardsList.Count; i++)
        {
            _cardScale = p_cardsList[i].size;

            p_cardsList[i].Sort(++_index, out _index);

            _cardPos.y = -_cardScale.y.Percentage(p_offset) * i;
            p_cardsList[i].transform.localPosition = _cardPos;

            p_cardsList[i].SetStartPos();
        }
    }
}
