﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Public

    public static Card SelectedCard;

    public static int Score;
    public static int Moves;
    public static int PreviousCardOrder;

    public GameData gameData;
    public bool canAlwaysAttach;

    public static bool Paused;

    #endregion

    #region Private

    private RaycastHit2D _hit;
    private GameObject _hittedObj;

    private float _inputTimer;

    #endregion

    private void Awake()
    {
        GameSingleton.GameManager = this;
        GameSingleton.GameData = this.gameData;
    }

    private void Update()
    {
        if (GameManager.Paused)
        {
            return;
        }

        if (InputManager.SingleInput)
        {
            this.Raycast(GameData.GetData<LayersData>().deckLayer, () =>
            {
                GameSingleton.CardsManager.deck.OnInput();
            });
        }

        if (InputManager.InputDown)
        {
            if (this._inputTimer < this.gameData.holdInputTime)
            {
                this._inputTimer += Time.deltaTime;
            }
            else
            {
                this.Raycast(GameData.GetData<LayersData>().cardLayer, () => 
                {
                    if (GameManager.SelectedCard == null)
                    {
                        GameManager.SelectedCard = this._hittedObj.GetComponent<Card>();
                        GameManager.SelectedCard.Select();
                    }
                });
            }
        }

        if (InputManager.InputUp)
        {
            this._inputTimer = 0;

            if (GameManager.SelectedCard != null)
            {
                if (!GameManager.SelectedCard.canAttach)
                {
                    GameManager.SelectedCard.Deselect();
                }
                else
                {
                    GameManager.SelectedCard.Attach();
                }

                GameManager.SelectedCard = null;
            }
        }

        if (GameManager.SelectedCard != null)
        {
            GameManager.SelectedCard.Move();
        }
    }

    private void Raycast(LayerMask p_layerMask, Action p_callback)
    {
        this._hit = Physics2D.Raycast(InputManager.InputPosition, Vector3.forward);

        if (this._hit.collider != null)
        {
            this._hittedObj = this._hit.collider.gameObject;

            if (this._hittedObj.layer.CheckLayer(p_layerMask))
            {
                p_callback?.Invoke();
            }
        }
    }

    public static void UpdateStats(int p_points, int p_moves)
    {
        GameManager.Score += p_points;
        GameManager.Moves += p_moves;

        if(GameManager.Score < 0)
        {
            GameManager.Score = 0;
        }

        GameSingleton.UIManager.UpdateHUD();
    }

    public static void Undo()
    {
        if(CardsManager.MovedCards.Count > 0 && !CardsManager.IsCardFlipping)
        {
            CardsManager.MovedCards.Last().Undo();
            GameManager.UpdateStats(0, -1);
        }
    }

    public static void ResetGame(bool p_newGame)
    {
        GameManager.Score = 0;
        GameManager.Moves = 0;

        GameSingleton.UIManager.UpdateHUD();
        GameSingleton.CardsManager.ResetCards(p_newGame);
    }

    public static void CheckCompletion()
    {
        if (CardsManager.Completed)
        {
            GameSingleton.UIManager.ChangeScreen(ScreenType.Finish);
        }
    }
}
