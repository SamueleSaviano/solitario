﻿using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public List<GameScreen> screens = new List<GameScreen>();

    public static GameScreen CurrentScreen;

    private HUD _HUD => this.GetScreenScript<HUD>();

    public GameScreen GetScreen(ScreenType p_type)
    {
        return this.screens.Find(_x => _x.type == p_type);
    }

    public T GetScreenScript<T>() where T : GameScreen
    {
        return (T)this.screens.Find(_x => _x.GetType() == typeof(T));
    }

    private void Awake()
    {
        GameSingleton.UIManager = this;
    }

    private void Start()
    {
        this.ChangeScreen(ScreenType.HUD);
    }

    public void AddScreen(GameScreen p_screen)
    {
        if(!this.screens.Contains(p_screen))
        {
            this.screens.Add(p_screen);
            p_screen.ExitScreen();
        }
    }

    public void ChangeScreen(ScreenType p_newScreen)
    {
        if(UIManager.CurrentScreen != null)
        {
            UIManager.CurrentScreen.ExitScreen();
        }

        UIManager.CurrentScreen = this.GetScreen(p_newScreen);
        UIManager.CurrentScreen.EnterScreen();
    }

    public void UpdateHUD()
    {
        this._HUD.UpdateGUI();
    }
}
