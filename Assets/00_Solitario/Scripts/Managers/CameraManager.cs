﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static Camera GameCamera;

    public static float HalfWidth;
    public static float HalfHeight;

    public static float TopLimit;
    public static float LeftLimit;
    public static float RightLimit;

    public static float Width => CameraManager.HalfWidth * 2f;
    public static float Height => CameraManager.HalfHeight * 2f;

    public static float HOffset => CameraManager.Width.Percentage(GameSingleton.GameData.hScreenOffset);

    private void Awake()
    {
        CameraManager.GameCamera = Camera.main;
        CameraManager.GameCamera.transform.position = new Vector3(0, 0, -10f);

        CameraManager.HalfHeight = CameraManager.GameCamera.orthographicSize;
        CameraManager.HalfWidth = CameraManager.GameCamera.aspect * CameraManager.HalfHeight;

        CameraManager.TopLimit = CameraManager.HalfHeight;
        CameraManager.LeftLimit = -CameraManager.HalfWidth + CameraManager.HOffset;
        CameraManager.RightLimit = CameraManager.HalfWidth - CameraManager.HOffset;
    }
}
