using System;
using UnityEngine;

public class GameSingleton : Singleton<GameSingleton> 
{
	protected GameSingleton() {}

    #region CONSTANTS

    #endregion

    #region PUBLIC_VARIABLES

    public static GameManager GameManager;
    public static UIManager UIManager;
    public static InputManager InputManager;
    public static CardsManager CardsManager;

    public static GameData GameData;

    #endregion

    #region PROTECTED_VARIABLES

    #endregion

    #region PRIVATE_VARIABLES

    #endregion

    #region PUBLIC_COMMANDS

    #endregion

    #region PUBLIC_METHODS

    #endregion

    #region PRIVATE_METHODS
    
    #endregion

    #region PUBLIC_CLASSES
    
    [Serializable]
    public class SuitSprite
    {
        public CardSuit suit;
        public Sprite sprite;
    }

    public class CardAspect
    {
        public CardSuit cardSuit;

        public Color color;
        public Sprite number;
        public Sprite suit;

        public int value;
    }

    [Serializable]
    public class Move
    {
        public Card card;
        public int moves;

        public void Undo()
        {
            this.card.Undo();
            this.moves--;

            if(this.moves == 0)
            {
                CardsManager.MovedCards.Remove(this);
            }
        }
    }

    #endregion
}

#region ENUMS

#region Cards

public enum CardSuit
{
    Hearts,
    Tiles,
    Clovers,
    Pikes
}

public enum CardColor
{
    Red,
    Black
}

public enum CardOrientation
{
    Front,
    Back
}

public enum CardParent
{
    Deck,
    Column,
    Base
}

#endregion

#region UI

public enum ScreenType
{
    HUD,
    Pause,
    Finish
}

#endregion

#endregion
