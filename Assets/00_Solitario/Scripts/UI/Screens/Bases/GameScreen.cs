﻿using UnityEngine;

public class GameScreen : MonoBehaviour
{
    public ScreenType type;

    protected virtual void Awake()
    {
        GameSingleton.UIManager.AddScreen(this);
    }

    public virtual void EnterScreen()
    {
        this.gameObject.SetActive(true);
    }

    public virtual void ExitScreen()
    {
        this.gameObject.SetActive(false);
    }
}
