﻿

public class PauseScreen : GameScreen
{
    protected override void Awake()
    {
        base.Awake();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();

        GameManager.Paused = true;
    }

    public override void ExitScreen()
    {
        base.ExitScreen();

        GameManager.Paused = false;
    }

    public void ReplayButton()
    {
        GameManager.ResetGame(false);
        this.ResumeButton();
    }

    public void ResetButton()
    {
        GameManager.ResetGame(true);
        this.ResumeButton();
    }

    public void ResumeButton()
    {
        GameSingleton.UIManager.ChangeScreen(ScreenType.HUD);
    }
}
