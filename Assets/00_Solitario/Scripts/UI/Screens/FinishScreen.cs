﻿

public class FinishScreen : GameScreen
{
    public UILabel scoreLabel;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();

        this.scoreLabel.text = GameManager.Score.ToString();
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void ResetButton()
    {
        GameManager.ResetGame(true);
        GameSingleton.UIManager.ChangeScreen(ScreenType.HUD);
    }
}
