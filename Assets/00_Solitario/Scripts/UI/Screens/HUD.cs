﻿using UnityEngine;

public class HUD : GameScreen
{
    private Vector3 _statsPosition;

    public UIWidget statsContainer;

    [Space]

    public UILabel scoreLabel;
    public UILabel movesLabel;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();
        this.UpdateGUI();
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void UpdateGUI()
    {
        this.scoreLabel.text = $"Punti\n{GameManager.Score}";
        this.movesLabel.text = $"Mosse\n{GameManager.Moves}";
    }

    public void PauseButton()
    {
        GameSingleton.UIManager.ChangeScreen(ScreenType.Pause);
    }

    public void UndoButton()
    {
        GameManager.Undo();
    }
}
