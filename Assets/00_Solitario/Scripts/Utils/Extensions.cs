﻿using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static T Random<T>(this List<T> p_list)
    {
        return p_list[UnityEngine.Random.Range(0, p_list.Count)];
    }

    public static T Last<T>(this List<T> p_list)
    {
        if (p_list.Count > 0)
        {
            return p_list[p_list.Count - 1];
        }

        return default(T);
    }

    public static float Percentage(this float p_value, float p_percentage)
    {
        return p_value / 100f * p_percentage;
    }

    public static bool CheckLayer(this int p_layer, LayerMask p_layerToCheck)
    {
        int _mask = LayersData.GetMask(p_layer);
        return _mask == p_layerToCheck.value;
    }

    public static bool IsInRange(this float p_value, float p_min, float p_max, bool p_includeLimits = true)
    {
        if (p_includeLimits)
        {
            return p_value >= p_min && p_value <= p_max;
        }
        
        return p_value > p_min && p_value < p_max;
    }
}
